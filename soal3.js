function hitung_huruf_vokal(vocal) {
    var text = vocal.toLowerCase();
    var jumlah = 0;
    for (var i = 0; i < text.length; i++) {
        switch (text[i]) {
            case 'a':
                jumlah++
                break;
            case 'i':
                jumlah++
                break;
            case 'u':
                jumlah++
                break;
            case 'e':
                jumlah++
                break;
            case 'o':
                jumlah++
                break;
            default:
                break;
        }
        
    }

    return jumlah
}

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)